#!/bin/sh
# From /usr/share/doc/acmetool/README.Debian.gz

[ "$1" = "live-updated" ] || exit 42 # unsupported event
systemctl reload nginx.service
