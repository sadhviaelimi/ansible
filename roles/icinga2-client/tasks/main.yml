---
- name: Install icinga2 and monitoring plugins
  apt:
    name:
    - icinga2
    - monitoring-plugins
    - nagios-plugins-contrib
  tags: icinga2-client

- name: Allow outgoing trafic to parent Icinga2
  ufw:
    direction: out
    rule: allow
    proto: tcp
    to_ip: "{{hostvars[icinga2_parent].ansible_all_ipv4_addresses |ipaddr('10.0.0.0/24') |first |default(hostvars[icinga2_parent].ansible_default_ipv4.address)}}"
    to_port: 5665
  tags:
    - icinga2-client
    - firewall

- name: Allow inbound trafic from parent Icinga2
  ufw:
    direction: in
    rule: allow
    proto: tcp
    from_ip: "{{hostvars[icinga2_parent].ansible_all_ipv4_addresses |ipaddr('10.0.0.0/24') |first |default(hostvars[icinga2_parent].ansible_default_ipv4.address)}}"
    to_port: 5665
  tags:
    - icinga2-client
    - firewall

# TODO: this check could be improved
- name: Is icinga2 client configured? (checking presence of /etc/icinga2/pki/{{ansible_fqdn}}.crt)
  stat:
    path: /etc/icinga2/pki/{{ansible_fqdn}}.crt
  register: icinga2_client_configured
  tags: icinga2-client

# Configure icinga2 in client mode if not already done.

- block:
    - name: Generate new private key/certificate pair
      command: "icinga2 pki new-cert --cn {{ansible_fqdn}} --key /etc/icinga2/pki/{{ansible_fqdn}}.key --cert /etc/icinga2/pki/{{ansible_fqdn}}.crt"
      args:
        creates: "/etc/icinga2/pki/{{ansible_fqdn}}.crt"
      notify: Restart Icinga2
      tags: icinga2-client

    - name: Generate new private key
      command: "icinga2 pki new-cert --cn {{ansible_fqdn}} --csr {{ansible_fqdn}}.csr --key {{ansible_fqdn}}.key"
      args:
        chdir: "/etc/icinga2/pki"
        creates: "/etc/icinga2/pki/{{ansible_fqdn}}.csr"
      delegate_to: "{{icinga2_master}}"
      notify: Restart Icinga2
      tags: icinga2-client

    - name: Sign new certificate
      command: "icinga2 pki sign-csr --csr {{ansible_fqdn}}.csr --cert {{ansible_fqdn}}.crt"
      args:
        chdir: "/etc/icinga2/pki"
        creates: "/etc/icinga2/pki/{{ansible_fqdn}}.crt"
      delegate_to: "{{icinga2_master}}"
      notify: Restart Icinga2
      tags: icinga2-client

    - name: Fetch master's certificate
      fetch:
        src: "/etc/icinga2/pki/{{item}}"
        dest: /tmp/
        flat: yes
      with_items:
        - "{{hostvars[icinga2_master].ansible_fqdn}}.crt"
        - "{{ansible_fqdn}}.crt"
        - "{{ansible_fqdn}}.key"
        - ca.crt
      delegate_to: "{{icinga2_master}}"
      tags: icinga2-client

    - name: Push master's certificate to client
      copy:
        src: "/tmp/{{item}}"
        dest: /etc/icinga2/pki/
        mode: "0644"
        owner: nagios
        group: nagios
      with_items:
        - "{{hostvars[icinga2_master].ansible_fqdn}}.crt"
        - "{{ansible_fqdn}}.crt"
        - "{{ansible_fqdn}}.key"
        - ca.crt
      notify: Restart Icinga2
      tags: icinga2-client

    - name: Remove certificate on local machine
      file:
        name: "/tmp/{{item}}"
        state: absent
      with_items:
        - "{{hostvars[icinga2_master].ansible_fqdn}}.crt"
        - "{{ansible_fqdn}}.crt"
        - "{{ansible_fqdn}}.key"
        - ca.crt
      delegate_to: localhost
      become: no
      tags: icinga2-client

    #- name: Save certificate
    #  command: "icinga2 pki save-cert --trustedcert /etc/icinga2/pki/trusted-master.crt --host {{hostvars[icinga2_master].ansible_fqdn}} --cert /etc/icinga2/pki/grosminet.facil.services.crt --key /etc/icinga2/pki/grosminet.facil.services.key"
    #  notify: Restart Icinga2
    #  tags: icinga2-client

    #- name: Generate new ticket on master
    #  command: "icinga2 pki ticket --cn {{ansible_fqdn}}"
    #  register: icinga2_pki_ticket
    #  delegate_to: "{{icinga2_master}}"
    #  tags: icinga2-client

    #- name: Setup node
    #  command: "icinga2 node setup --ticket {{icinga2_pki_ticket.stdout}} --endpoint {{hostvars[icinga2_master].ansible_fqdn}} --zone {{ansible_fqdn}} --master_host {{hostvars[icinga2_master].ansible_fqdn}} --trustedcert /etc/icinga2/pki/{{hostvars[icinga2_master].ansible_fqdn}}.crt --accept-commands --accept-config"
    #  notify: Restart Icinga2
    #  tags: icinga2-client

    - name: Comment out conf.d/ inclusion in icinga2.conf
      replace:
        name: /etc/icinga2/icinga2.conf
        regexp: '^(include_recursive "conf.d")$'
        replace: '//\1'
      notify: Restart Icinga2
      tags: icinga2-client

    - name: Remove conf.d/ directory
      file:
        name: /etc/icinga2/conf.d/
        state: absent
      notify: Restart Icinga2
      tags: icinga2-client

  when: not icinga2_client_configured.stat.exists

- name: Enable api and checker feature
  icinga2_feature:
    name: "{{item}}"
  with_items:
    - api
    - checker
  when: icinga2_master is defined and icinga2_master != ansible_hostname
  notify: Restart Icinga2
  tags: icinga2-client

- name: Enable accept_config and accept_commands API options
  lineinfile:
    name: /etc/icinga2/features-available/api.conf
    line: "{{item}}"
    regexp: "{{item}}"
    insertbefore: "^}$"
  with_items:
    - "  accept_commands = true"
    - "  accept_config = true"
  when: icinga2_master is defined and icinga2_master != ansible_hostname
  notify: Restart Icinga2
  tags: icinga2-client

- name: Template zones.conf
  template:
    src: zones.conf.j2
    dest: /etc/icinga2/zones.conf
    mode: "0644"
  when: icinga2_master is defined and icinga2_master != ansible_hostname
  notify: Restart Icinga2
  tags: icinga2-client

- name: Copy additional command plugins
  copy:
    src: "{{item}}"
    dest: /usr/lib/nagios/plugins/
    mode: "0755"
  with_fileglob: plugins/*
  tags: icinga2-client

#- name: Add nagios user to group disk (for check_smart)
#  user:
#    name: nagios
#    groups: disk
#    append: yes
#  when: ansible_virtualization_role == "host"
#  tags: icinga2-client
#  notify: Restart Icinga2
