apply Service "ping4" {
  import "generic-service"
  check_command = "ping4"

  // Run check only on public IP address
  assign where host.vars.hosted_by && host.vars.ping4_address || !host.vars.hosted_by

  // Run check from the master
  zone = host.vars.master_zone

  vars.ping_address = host.vars.ping4_address || host.address
}

apply Service "ping6" {
  import "generic-service"
  check_command = "ping6"

  assign where host.address6 || host.vars.ping6_address

  // Run check from the master
  zone = host.vars.master_zone

  vars.ping_address = host.vars.ping6_address || host.address6
}

apply Service "disk" {
  import "generic-service"
  check_command = "disk"

  //specify where the check is executed
  command_endpoint = host.vars.client_endpoint

  assign where host.vars.client_endpoint
}

apply Service "dns" {
  import "generic-service"
  check_command = "dns"

  command_endpoint = host.vars.client_endpoint
  assign where host.vars.client_endpoint

  vars.dns_lookup = "facil.qc.ca"
  vars.dns_expected_answers = "149.56.80.56"
}

apply Service "load" {
  import "generic-service"
  check_command = "load"

  command_endpoint = host.vars.client_endpoint
  // no check_load on LXC containers
  assign where host.vars.client_endpoint && (host.vars.virtualization_role == "host" || host.vars.virtualization_role == "guest" && host.vars.virtualization_type == "kvm")
}

apply Service "mailq" {
  import "generic-service"
  check_command = "mailq"

  command_endpoint = host.vars.client_endpoint
  assign where host.vars.client_endpoint

  vars.mailq_critical = 100
  vars.mailq_warning = 10
}

apply Service "ntp_time" {
  import "generic-service"
  check_command = "ntp_time"

  command_endpoint = host.vars.client_endpoint
  assign where host.vars.client_endpoint
}

apply Service "procs" {
  import "generic-service"
  check_command = "procs"

  command_endpoint = host.vars.client_endpoint
  assign where host.vars.client_endpoint
}

apply Service "smtp" {
  import "generic-service"
  check_command = "smtp"

  command_endpoint = host.vars.client_endpoint
  assign where host.vars.client_endpoint
}

apply Service "swap" {
  import "generic-service"
  check_command = "swap"

  command_endpoint = host.vars.client_endpoint
  assign where host.vars.client_endpoint && host.vars.has_swap != "false"
}

apply Service "ssh" {
  import "generic-service"
  check_command = "ssh"

  assign where host.address
}

apply Service "users" {
  import "generic-service"
  check_command = "users"

  command_endpoint = host.vars.client_endpoint
  assign where host.vars.client_endpoint

  vars.users_wgreater = 5
  vars.users_cgreater = 20
}

apply Service "mem" {
  import "generic-service"
  check_command = "mem"

  command_endpoint = host.vars.client_endpoint

  vars.mem_cache = "true"
  vars.mem_warning = "10"
  vars.mem_critical = "5"
  vars.mem_free = "true"

  // no check_load on LXC containers
  assign where host.vars.client_endpoint && (host.vars.virtualization_role == "host" || host.vars.virtualization_role == "guest" && host.vars.virtualization_type == "kvm")
}

apply Service "syslog" {
  import "generic-service"
  check_command = "file_age"

  //specify where the check is executed
  command_endpoint = host.vars.client_endpoint

  assign where host.vars.client_endpoint

  vars.file_age_file = "/var/log/syslog"
  vars.file_age_warning_time = 43200
  vars.file_age_critical_time = 86400
}

apply Service "cron" {
  import "generic-service"
  check_command = "procs"

  //specify where the check is executed
  command_endpoint = host.vars.client_endpoint

  assign where host.vars.client_endpoint

  vars.procs_critical = "1:"
  vars.procs_state = "RSSs"
  vars.procs_user = "root"
  vars.procs_command = "cron"
}

apply Service "apt" {
  import "generic-service"
  check_command = "apt"
  // unattended-upgrade runs once a day so if packages don't get upgraded in
  // less than 24h, the check must raise an alert.
  check_interval = 1d
  retry_interval = 1h
  max_check_attempts = 24

  //specify where the check is executed
  command_endpoint = host.vars.client_endpoint

  assign where host.vars.client_endpoint
}

apply Service "cluster" {
  import "generic-service"
  check_command = "cluster"

  command_endpoint = host.vars.client_endpoint
  assign where host.vars.client_endpoint
}

apply Service "cluster-health" {
  import "generic-service"
  check_command = "cluster-zone"

  display_name = "cluster-health-" + host.display_name
  vars.cluster_zone = host.name

  assign where host.vars.client_endpoint && host.vars.icinga2_role != "master"
}

apply Service "mariadb" {
  import "generic-service"
  check_command = "mysql"

  command_endpoint = host.vars.client_endpoint
  assign where host.vars.db_type == "mysql" || host.vars.db_type == "mariadb"
  vars.mysql_file = "~nagios/.my.cnf"
  # Plugin seems not to read hostname from ~nagios/.my.cnf :-(
  vars.mysql_hostname = "127.0.0.1"
}

apply Service "postgresql" {
  import "generic-service"
  check_command = "pgsql"

  command_endpoint = host.vars.client_endpoint
  assign where host.vars.db_type == "postgresql"
  vars.pgsql_hostname = "/var/run/postgresql/"
}

apply Service "mongodb" {
  import "generic-service"
  check_command = "mongodb"

  command_endpoint = host.vars.client_endpoint
  assign where host.vars.db_type == "mongodb"
}

//apply Service for (device in host.vars.devices) {
//  import "generic-service"
//  check_command = "smart"
//  command_endpoint = host.vars.client_endpoint
//  display_name = "smart_" + device
//  vars.smart_device = "/dev/" + device
//  check_interval = 1d
//  retry_interval = 1m
//
//  ignore where device == "sr0" || match("dm-*", device) || match("md*", device)
//}

apply Service for (zone in host.vars.knot_zones) {
  import "generic-service"
  check_command = "dig"
  display_name = "knot_" + zone

  vars.dig_server = host.vars.ping4_address || host.address
  vars.dig_lookup = zone

  // Run check from the master
  zone = host.vars.master_zone

  assign where "nameservers" in host.groups
}

apply Service "http-" for (site => config in host.vars.web_sites) {
  import "generic-service"
  check_command = "http"
  display_name = "http_" + site

  // Default values
  vars.http_sni = "true"
  vars.http_extendedperfdata = "true"
  vars.http_expect = "200"
  vars.http_address = host.vars.ping4_address || host.address
  // Only used to have a link to the site in Icinga web 2
  //vars.notes_url = "http(s)://" + vars.http_vhost + vars.http_uri

  // Load specific config
  vars += config
  #vars.http_uri = "{{site.uri |default('/')}}"
  #vars.http_vhost = "{{site.domain}}"
  #vars.http_ssl = "true"

  // Run check from the master
  zone = host.vars.master_zone

  // Report incident to Cachet
  event_command = "cachet-notify"
  vars.cachet_component = site
}

apply Service "cert-" for (site => config in host.vars.web_sites) {
  import "generic-service"
  check_command = "http"
  display_name = "cert_" + site
  check_interval = 1d
  retry_interval = 1m

  // Default values
  vars.http_certificate = "7,3"
  vars.http_sni = "true"
  vars.http_address = host.vars.ping4_address || host.address

  // Load specific config
  vars += config
  #vars.http_vhost = "{{site.domain}}"

  // Run check from the master
  zone = host.vars.master_zone

  assign where config.http_ssl
}
